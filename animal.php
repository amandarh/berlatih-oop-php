<?php

class animals
{
    public function __construct($name, $legs, $cold_blooded)
    {
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
    public function intro()
    {
        echo "Binatang {$this->name} mempunyai {$this->legs} kaki dan darah dingin = {$this->cold_blooded} <br>";
    }
}
