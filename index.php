<?php
//require("animal.php");
require("ape.php");
require("frog.php");

$sheep = new animals("shaun", 2, "false");
$sheep->intro();

echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br><br>"; // false


$sungokong = new Ape("kera sakti", 2, "false");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk", 4, "false");
$kodok->jump(); // "hop hop"